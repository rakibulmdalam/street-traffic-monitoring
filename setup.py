from setuptools import setup, find_packages

# Setup configuration for the tool
setup(
    name='YOLO_VEHICLE_MONITOR',
    version='0.1',
    long_description="",
    packages=find_packages(),
    include_package_data=False,
    zip_safe=False,
    install_requires=[
        'numpy', # scientific computing, OSI Approved (BSD)
        'requests', # http integreation
        'pandas', # Powerful data structures for data analysis, time series, and statistics, BSD
        'scipy', # Scientific Library for Python, BSD License (BSD)
        'imutils', # image manipulation
        'numba', # spatial calculation
        'sklearn', # learning
        'filterpy', # for kalman filter
        'elasticsearch>=6.0.0,<7.0.0' 
    ]
)
# import the necessary packages
import numpy as np
import argparse
import imutils
import time
import cv2
import os
import glob
from scipy.spatial import distance
import math
import json
from elasticsearch import Elasticsearch

from sort import *

class VehicleTracker:
    def __init__(self, server='es'):
        self.es = 'elasticsearch address here'
        if server == 'es':
            try:
                self.es = Elasticsearch([es_address])
                # ignore 400 cause by IndexAlreadyExistsException when creating an index
                self.es.indices.create(index='vehicle-detection', ignore=400)
                self.es_active = True
            except:
                self.es_active = False
                print('Elastic connection error. Data will not be saved in cloud')

        self.tracker = Sort()
        self.memory = {}
        self.W = None
        self.H = None
        self.input()
        self._init_yolo()

    
    def send_online(self, doc, type="Elasticsearch"):
        if type == "Elasticsearch" and self.es_active:
            try:
                es = self.es
                es.indices.create(index='vehicle-detection', ignore=400)
                res = es.index(index="vehicle-detection", doc_type='lane', body=doc)
                print(res)
            except:
                print('Elastic error. Data might not be saved')


    def interactive_input(self, resize=60):
        self.calcualte_FPS()
        self.lane_info()
    
    def calcualte_FPS(self):
        startFPS = time.time()
        frameIndex = 0
        for i in range(100):
            (grabbed, frame) = self.vs.read()
            if not grabbed:
                print('Could not read source video/stream... \nProgram exiting...')
                exit(0)
            frameIndex += 1 

        endFPS = time.time()
        seconds = endFPS - startFPS
        self.fps  = frameIndex / seconds

    
    def show_stream(self):
        print('Displaying stream....')
        while True:
            (grabbed, frame) = self.vs.read()
            if not grabbed:
                print('Could not read source video/stream... \nProgram exiting...')
                exit(0)
                
            frame = self.resize_frame(frame)
            cv2.imshow('Stream',frame)
            if cv2.waitKey(1) == 27:
                break
        print("Streaming stopped....")

    def lane_info(self):
        geometry_file = input("Please provide the filename which contains geometric information: ")
        with open(geometry_file, 'r') as file:
            data = file.read()
            self.geo = json.loads(data)
        print("# of lanes (all streets combined) provided: {}".format(len(self.geo)))
        

    def input(self):
        ap = argparse.ArgumentParser()
        ap.add_argument("-i", "--input", help="path to input video", default='input/idp_sample.mp4')
        ap.add_argument("-o", "--output", help="path to output file", default='output.csv')
        ap.add_argument("-r", "--resize", help="resize percentage", default=100)
        ap.add_argument("-d", "--display", help="display stream to view lanes", required=True)
        ap.add_argument("-y", "--yolo", help="base path to YOLO directory", default='yolo-coco')
        ap.add_argument("-c", "--confidence", type=float, default=0.5, help="minimum probability to filter weak detections")
        ap.add_argument("-t", "--threshold", type=float, default=0.3, help="threshold when applyong non-maxima suppression")
        self.args = vars(ap.parse_args())

        self.vs = cv2.VideoCapture(self.args["input"])
        self.f = open("output/"+self.args["output"], "a")
        
        if eval(str(self.args["display"])):
            self.show_stream()
            
        self.interactive_input()
    
    
    def intersect(self, A,B,C,D):
	    return self.ccw(A,C,D) != self.ccw(B,C,D) and self.ccw(A,B,C) != self.ccw(A,B,D)

    
    def ccw(self, A,B,C):
	    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

    def _init_yolo(self):
        # derive the paths to the YOLO weights and model configuration
        self.weightsPath = os.path.sep.join([self.args["yolo"], "yolov3.weights"])
        self.configPath = os.path.sep.join([self.args["yolo"], "yolov3.cfg"])

        print("[INFO] loading YOLO from disk...")
        
        self.net = cv2.dnn.readNetFromDarknet(self.configPath, self.weightsPath)
        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

    
    def resize_frame(self, frame):
        scale_percent = int(self.args["resize"])
        if scale_percent < 100:
            width = int(frame.shape[1] * scale_percent / 100)
            height = int(frame.shape[0] * scale_percent / 100)
            dim = (width, height)
            # resize image
            frame = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)

        return frame

    def process_layerOutputs(self, layerOutputs, W, H):
        boxes = []
        confidences = []
        classIDs = []

        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                if confidence > self.args["confidence"]:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        return boxes, confidences, classIDs
    

    def detect(self, frame):
        blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
            swapRB=True, crop=False)
        self.net.setInput(blob)
        return self.net.forward(self.ln)


    def current_vehicles(self, frame):
        layerOutputs = self.detect(frame)
        boxes, confidences, classIDs = self.process_layerOutputs(layerOutputs, self.W, self.H)
        return cv2.dnn.NMSBoxes(boxes, confidences, self.args["confidence"], self.args["threshold"]), boxes, confidences
        

    def track(self):
        centroids = {}
        frameIndex = 0
        tracks = []

        vehicleCounter = []        
        for i in range(len(self.geo)):
            vehicleCounter.append(0)
        
        while True:
            (grabbed, frame) = self.vs.read()
            if not grabbed:
                print('Could not read source video/stream... \nProgram exiting...')
                self.vs.release()
                self.f.close()
                exit(0)
            
            frame = self.resize_frame(frame)

            if self.W is None or self.H is None:
                (self.H, self.W) = frame.shape[:2]

            
            idxs, boxes, confidences = self.current_vehicles(frame)
            
            dets = []
            if len(idxs) > 0:
                # loop over the indexes we are keeping
                for i in idxs.flatten():
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])
                    dets.append([x, y, x+w, y+h, confidences[i]])

            np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
            dets = np.asarray(dets)

            #print("# of objects being monitored currently {}".format(len(dets)))
            if len(dets) > 0:
                tracks = self.tracker.update(dets)

            boxes = []
            indexIDs = []
            c = []
            previous = self.memory.copy()
            self.memory = {}

            for track in tracks:
                boxes.append([track[0], track[1], track[2], track[3]])
                indexIDs.append(int(track[4]))
                self.memory[indexIDs[-1]] = boxes[-1]

            if len(boxes) > 0:
                i = int(0)
                for box in boxes:
                    # extract the bounding box coordinates
                    (x, y) = (int(box[0]), int(box[1]))
                    (w, h) = (int(box[2]), int(box[3]))

                    if indexIDs[i] in previous:
                        previous_box = previous[indexIDs[i]]
                        (x2, y2) = (int(previous_box[0]), int(previous_box[1]))
                        (w2, h2) = (int(previous_box[2]), int(previous_box[3]))
                        p0 = (int(x + (w-x)/2), int(y + (h-y)/2))
                        p1 = (int(x2 + (w2-x2)/2), int(y2 + (h2-y2)/2))
                        
                        if indexIDs[i] in centroids:
                            centroids[indexIDs[i]].append(p0)
                            
                        else:
                            centroids[indexIDs[i]] = [p0]

                        
                        for c, l in enumerate(self.geo, start=0):
                            if self.intersect(p0, p1, (int(l['exit'][0][0]), l['exit'][0][1]), (int(l['exit'][1][0]), l['exit'][1][1])):
                                vehicleCounter[c] = vehicleCounter[c] + 1

                                coords = [list(centroids[indexIDs[i]][0]), list(centroids[indexIDs[i]][-1])]
                                number_of_frames = len(centroids[indexIDs[i]])
                                
                                del centroids[indexIDs[i]]

                                packet = {
                                    'lane': c+1,
                                    'frameIndex': frameIndex,
                                    'speed': measure_velocity(coords, number_of_frames, c),
                                    'time': time.time()
                                }
                                print(packet) 
                                self.send_online(packet, "Elasticsearch")
                                
                                break
                    
                    i += 1

            frameIndex += 1

        self.vs.release()
        self.f.close()

    
    def midpoint(self, x1, y1, x2, y2):
        return ((x1 + x2)/2, (y1 + y2)/2)
        

    def dist(self, x,y):   
        return numpy.sqrt(numpy.sum((x-y)**2))
        
    def measure_velocity(self, coords, number_of_frames, lane):
        velocity = -1
        try:
            # distance between entry and exit line for the lane
            # calculate mid point of entry and exit
            mid_entry = self.midpoint(self.geo['lane']['entry'][0][0], self.geo['lane']['entry'][0][1], self.geo['lane']['entry'][1][0], self.geo['lane']['entry'][1][1])
            mid_exit = self.midpoint(self.geo['lane']['exit'][0][0], self.geo['lane']['exit'][0][1], self.geo['lane']['exit'][1][0], self.geo['lane']['exit'][1][1])
            
            pixel_distance = self.dist(mid_entry, mid_exit)
            actual_distance = self.geo['lane']['length']
            distance_per_pixel = actual_distance / pixel_distance
            
            centroid_pixel_distance = self.dist(coords[0], coords[1])
            centroid_actual_distance = centroid_pixel_distance * distance_per_pixel
            
            # assuming vehicle is detected in each frame
            time = number_of_frames / self.fps
            velocity = centroid_actual_distance / time
        except:
            print('Could not measure velocity. Check for errors in geo information or centroid tracking..')
        
        return velocity
        

if __name__ == "__main__":
    VT = VehicleTracker()
    VT.track()